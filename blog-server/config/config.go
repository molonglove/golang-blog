package config

import "fmt"

type SystemConfig struct {
	MySQL      `yaml:"mysql"`
	System     `yaml:"system"`
	Logger     `yaml:"logger"`
	JWT        `yaml:"jwt"`
	FileConfig `yaml:"file"`
}

type Captcha struct {
	KeyLong   int `yaml:"key-long"`
	ImgWidth  int `yaml:"img-width"`
	ImgHeight int `yaml:"img-height"`
}

type System struct {
	Part   string `yaml:"part"`   // 地址
	Status string `yaml:"status"` // 状态
}

type FileConfig struct {
	BasePath string `yaml:"basePath"` // 根路径
}

type JWT struct {
	SignKey     string `yaml:"sign-key"`
	ExpiresTime int64  `yaml:"expires-time"`
	Issuer      string `yaml:"issuer"`
}

type MySQL struct {
	Category     string `yml:"category"`       // 数据库类型
	Prefix       string `yml:"prefix"`         // 表前缀
	DataBase     string `yml:"database"`       // 数据库名字
	Ip           string `yml:"ip"`             // IP地址
	Part         string `yml:"part"`           // 端口
	Username     string `yml:"username"`       // 用户名
	Password     string `yml:"password"`       // 密码
	Print        bool   `yml:"print"`          // 是否输出SQL
	MaxIdleConns int    `yml:"max-idle-conns"` //
	MaxOpenConns int    `yml:"max-open-conns"` // 最大连接数
}

func (m *MySQL) ToString() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		m.Username,
		m.Password,
		m.Ip,
		m.Part,
		m.DataBase,
	)
}

type Logger struct {
	Path string `yml:"path"` // 日志文件路径
}
