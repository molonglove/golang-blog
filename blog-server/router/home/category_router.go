package home

import (
	"blog-server/api/home"
	"github.com/gin-gonic/gin"
)

func InitHomeCategoryRouter(r *gin.RouterGroup) {
	category := home.NewCategoryHomeApi()
	group := r.Group("category")
	{
		group.GET("all", category.CategoryList)
	}
}
