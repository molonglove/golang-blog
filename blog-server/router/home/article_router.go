package home

import (
	"blog-server/api/home"
	"github.com/gin-gonic/gin"
)

func InitHomeArticleRouter(r *gin.RouterGroup) {
	article := home.NewArticleApi()
	group := r.Group("article")
	{
		group.POST("page", article.ArticlePage)
		group.GET("tags", article.ArticleTags)       // 获取标签
		group.GET("detail", article.ArticleDetail)   // 获取某一个文章
		group.GET("archive", article.ArticleArchive) // 文章归档
	}
}
