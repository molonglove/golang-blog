package admin

import (
	"blog-server/api/admin"
	"github.com/gin-gonic/gin"
)

func InitAdminCategoryRouter(r *gin.RouterGroup) {
	category := admin.NewCategoryController()
	group := r.Group("category")
	{
		group.GET("all", category.CategoryList)
		group.POST("pages", category.CategoryPages)
		group.POST("add", category.CategoryAdd)
		group.POST("update", category.CategoryUpdate)
		group.GET("delete", category.CategoryDel)
	}
}
