package admin

import (
	"blog-server/api/admin"
	"github.com/gin-gonic/gin"
)

func InitArticleFileRouter(r *gin.RouterGroup) {
	article := admin.NewArticleController()
	group := r.Group("article")
	{
		group.POST("list", article.ArticleList)        // 分页获取数据
		group.POST("upload", article.ArticleUploadWay) //通过上传的方式创建文章
		group.POST("add", article.ArticleAddWay)       // 通过编辑的方式增加文章
		group.DELETE("delete", article.ArticleDelete)  // 文章删除
		group.GET("overhead", article.ArticleOverhead) // 文章的置顶
		group.PUT("update", article.ArticleUpdate)     // 文章的更新
		group.GET("backup", article.ArticleBackup)     // 文章的备份
		group.GET("content", article.ArticleContent)   // 文章的内容
	}
}
