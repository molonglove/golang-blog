package admin

import (
	"blog-server/api/admin"
	"github.com/gin-gonic/gin"
)

// InitAdminFileRouter 文件同步路由
func InitAdminFileRouter(r *gin.RouterGroup) {
	fileController := admin.NewFileController()
	group := r.Group("file")
	{
		group.POST("upload", fileController.UploadFile)    // 文件数据上传
		group.GET("download", fileController.DownloadFile) // 文件下载
	}
}
