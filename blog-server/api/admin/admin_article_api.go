package admin

import (
	"blog-server/models/views"
	"blog-server/service"
	"blog-server/utils"
	"blog-server/utils/msg"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

type ArticleController struct {
	*service.ArticleService
}

func NewArticleController() *ArticleController {
	return &ArticleController{
		service.NewArticleService(),
	}
}

// ArticleList 文章列表
func (article *ArticleController) ArticleList(c *gin.Context) {
	var param views.ArticleListVo
	if err := c.ShouldBind(&param); err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("查询参数异常"))
		return
	}
	list, err := article.ArticleService.ArticleAdminList(param)
	if err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed(err.Error()))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success(list))
}

// ArticleUploadWay 增加文章信息通过上传文件
func (article *ArticleController) ArticleUploadWay(c *gin.Context) {
	var params views.AdminArticleUploadVo
	if err := c.ShouldBind(&params); err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("查询参数异常"))
		return
	}
	flag, err := article.ArticleService.ArticleUploadByFile(params)
	if err != nil || !flag {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed(err.Error()))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success("创建文章成功"))
}

// ArticleAddWay 增加文章通过编辑方式
func (article *ArticleController) ArticleAddWay(c *gin.Context) {

}

// ArticleDelete 文章的删除（支持批量删除）
func (article *ArticleController) ArticleDelete(c *gin.Context) {
	ids, err := c.GetQuery("ids")
	if !err {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("参数不存在"))
		return
	}
	isLists := strings.Split(ids, "-")
	if err := article.ArticleService.ArticleDeleteByIds(isLists); err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("删除文章数据失败"))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success("删除文章成功"))
}

// ArticleOverhead 文章的顶置
func (article *ArticleController) ArticleOverhead(c *gin.Context) {
	id, idOk := c.GetQuery("id")
	flag, flogOk := c.GetQuery("flag")
	if !idOk && !flogOk {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("请求参数不正确"))
		return
	}
	parseBool, _ := utils.ParseBool(flag)
	if err := article.ArticleService.ArticleOverHeadFlag(id, parseBool); err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("设置文章顶置失败"))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success("设置文章顶置状态成功"))
}

// ArticleContent 文章内容
func (a *ArticleController) ArticleContent(c *gin.Context) {
	id, idOk := c.GetQuery("id")
	if !idOk {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("参数不存在"))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success(a.ArticleService.ArticleContent(id)))
}

// ArticleUpdate 文章的更新
func (article *ArticleController) ArticleUpdate(c *gin.Context) {

}

// ArticleBackup 文章的备份
func (article *ArticleController) ArticleBackup(c *gin.Context) {

}
