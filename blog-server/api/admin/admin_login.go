package admin

import (
	"blog-server/models/views"
	"blog-server/service"
	"blog-server/utils/msg"
	"github.com/gin-gonic/gin"
	"net/http"
)

type UserController struct {
	userService *service.UserService
}

func NewUserController() *UserController {
	return &UserController{service.NewUserService()}
}

// Login 用户登录
func (u *UserController) Login(c *gin.Context) {
	var params views.LoginVo
	if err := c.ShouldBind(&params); err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("查询参数异常"))
		return
	}
	login, err := u.userService.UserLogin(params)
	if err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed(err.Error()))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success(login))

}
