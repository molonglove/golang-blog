package admin

import (
	"blog-server/models/views"
	"blog-server/service"
	"blog-server/utils"
	"blog-server/utils/msg"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

type CategoryController struct {
	*service.CategoryService
}

func NewCategoryController() *CategoryController {
	return &CategoryController{service.NewCategoryService()}
}

// CategoryPages 分页获取分类数据
func (category *CategoryController) CategoryPages(c *gin.Context) {
	var param views.CategoryPagesVo
	if err := c.ShouldBind(&param); err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("查询参数异常"))
		return
	}
	page, err := category.CategoryService.CategoryPages(param)
	if err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed(err.Error()))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success(page))
}

// CategoryList 获取所有的分类信息
func (category *CategoryController) CategoryList(c *gin.Context) {
	all, err := category.CategoryService.CategorySelectAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed(err.Error()))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success(all))
}

// CategoryAdd 分类增加
func (category *CategoryController) CategoryAdd(c *gin.Context) {
	var param views.CategoryAddVo
	if err := c.ShouldBind(&param); err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("增加分类参数异常"))
		return
	}
	flag, err := category.CategoryService.CategoryInsert(param)
	if err != nil || !flag {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("增加分类信息失败"))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success("增加分类信息成功"))
}

// CategoryUpdate 分类增加
func (category *CategoryController) CategoryUpdate(c *gin.Context) {
	var param views.CategoryUpdateVo
	if err := c.ShouldBind(&param); err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("修改分类参数异常"))
		return
	}
	flag, err := category.CategoryService.CategoryUpdate(param)
	if err != nil || !flag {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("修改分类信息失败"))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success("修改分类信息成功"))
}

// CategoryDel 删除（支持批量）
func (category *CategoryController) CategoryDel(c *gin.Context) {
	idsStr := c.Query("ids")
	ids := utils.TypeChange(idsStr, ",")
	if len(ids) <= 0 {
		log.Println("要删除的分类信息为空")
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("获取请求参数失败"))
		return
	}
	if _, err := category.CategoryService.CategoryDelete(ids); err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("删除分类信息失败"))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success("删除分类信息成功"))
}
