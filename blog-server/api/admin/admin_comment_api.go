package admin

import (
	"blog-server/models/views"
	"blog-server/service"
	"blog-server/utils/msg"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
	"strings"
)

type CommentController struct {
	commentService *service.CommentService
}

func NewCommentController() *CommentController {
	return &CommentController{
		commentService: service.NewCommentService(),
	}
}

// CommentPages 评论信息分页展示
func (comm *CommentController) CommentPages(c *gin.Context) {
	var params views.AdminCommentRespVo
	if err := c.ShouldBind(&params); err != nil {
		logrus.Errorf("评论信息分页展示：参数获取失败：%s", err)
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("查询参数异常"))
		return
	}
	result, err := comm.commentService.CommentList(params)
	if err != nil {
		logrus.Errorf("评论信息分页展示：获取评论数据失败：%s", err)
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("获取评论数据失败"))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success(result))

}

// CommentDelete 删除评论（支持批量删除）
func (comm *CommentController) CommentDelete(c *gin.Context) {
	ids, flag := c.GetQuery("ids")
	if !flag {
		logrus.Errorf("删除评论：请求ids参数丢失")
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("查询参数丢失"))
		return
	}
	num, err := comm.commentService.CommentBatchDel(strings.Split(ids, "-"))
	if err != nil {
		logrus.Errorf("删除评论：删除数据失败")
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("删除评论数据失败"))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success(fmt.Sprintf("删除%d条评论信息成功", num)))
}
