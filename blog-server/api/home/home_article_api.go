package home

import (
	"blog-server/models/views"
	"blog-server/service"
	"blog-server/utils/msg"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type ArticleApi struct {
	*service.ArticleService
}

func NewArticleApi() *ArticleApi {
	return &ArticleApi{service.NewArticleService()}
}

// ArticlePage 条件搜索分页查询
func (article *ArticleApi) ArticlePage(c *gin.Context) {
	var param *views.HomeArticlePageVo
	if err := c.ShouldBindJSON(&param); err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("参数解析错误"))
		return
	}
	list, err := article.ArticleService.ArticleSelectPages(param)
	if err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("获取数据失败"))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success(list))
}

// ArticleTags 文章标签
func (article *ArticleApi) ArticleTags(c *gin.Context) {
	data, err := article.ArticleService.ArticleTagsAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("获取数据失败"))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success(data))
}

// ArticleDetail 文章详情
func (article *ArticleApi) ArticleDetail(c *gin.Context) {
	query := c.Query("id")
	id, _ := strconv.Atoi(query)
	detail, err := article.ArticleService.ArticleDetail(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed(err.Error()))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success(detail))
}

// ArticleArchive 文章归档信息
func (article *ArticleApi) ArticleArchive(c *gin.Context) {
	query := c.Query("year")
	info, err := article.ArticleService.ArticleArchive(query)
	if err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed(err.Error()))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success(info))
}
