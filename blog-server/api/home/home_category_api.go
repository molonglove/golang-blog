package home

import (
	"blog-server/service"
	"blog-server/utils/msg"
	"github.com/gin-gonic/gin"
	"net/http"
)

type CategoryHomeApi struct {
	*service.CategoryService
}

// NewCategoryHomeApi 创建
func NewCategoryHomeApi() *CategoryHomeApi {
	return &CategoryHomeApi{service.NewCategoryService()}
}

// CategoryList 获取分类列表
func (category *CategoryHomeApi) CategoryList(c *gin.Context) {
	data, err := category.CategoryService.CategorySelectAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed(err.Error()))
		return
	}
	c.JSON(http.StatusOK, msg.NewMsg().Success(data))
}
