package home

import (
	"blog-server/service"
	"blog-server/utils/msg"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
)

type CommentController struct {
	commentService *service.CommentService
}

func NewCommentController() *CommentController {
	return &CommentController{
		commentService: service.NewCommentService(),
	}
}

// CommentArticle 获取文章评论信息
func (comm *CommentController) CommentArticle(c *gin.Context) {
	id, flag := c.GetQuery("id")
	if !flag {
		logrus.Errorf("获取文章评论：请求id参数丢失")
		c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("查询参数丢失"))
		return
	}
	//result, err := comm.commentService.CommentByArticle(id)
	//if err != nil {
	//	logrus.Errorf("获取文章评论数据失败")
	//	c.JSON(http.StatusBadRequest, msg.NewMsg().Failed("获取文章评论数据失败"))
	//	return
	//}
	c.JSON(http.StatusOK, msg.NewMsg().Success(id))

}
