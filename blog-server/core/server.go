package core

import (
	"blog-server/global"
	"blog-server/initialize"
)

type server interface {
	ListenAndServe() error
}

func RunWindowsServer() {
	// 初始化路由
	Router := initialize.Routers()
	// 初始化服务
	s := initServer(global.Config.System.Part, Router)
	// 启动服务器
	s.ListenAndServe().Error()
}
