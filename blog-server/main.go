package main

import (
	"blog-server/core"
	"blog-server/global"
	"blog-server/initialize"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build main.go  ：mac

// set GOARCH=amd64
// set GOOS=linux
// go build   :win
func main() {
	// 解析参数
	if len(os.Args) > 1 {
		for idx, arg := range os.Args {
			fmt.Println("参数"+strconv.Itoa(idx)+" : ", arg)
		}
		arg := strings.Split(os.Args[1], "=")
		if len(arg) == 2 && arg[0] == "--config" {
			global.Config = initialize.InitProvConfig(arg[1])
		}
	} else {
		global.Config = initialize.InitDevConfig()
	}
	global.Engine = initialize.InitDB()
	defer global.Engine.Close()
	core.RunWindowsServer()
}
