package initialize

import (
	admin2 "blog-server/api/admin"
	"blog-server/middleware"
	"blog-server/router"
	"blog-server/router/admin"
	"blog-server/router/home"
	"github.com/gin-gonic/gin"
)

func Routers() *gin.Engine {
	engine := gin.Default()
	engine.Use(middleware.Logger())
	// 跨域配置
	engine.Use(middleware.Cors())

	public := engine.Group("api")
	{
		router.InitBaseRouter(public) // 公共接口。无需要认证
		homeGroup := public.Group("home")
		{
			router.InitProjectRouter(homeGroup)    // 项目管理相关的接口
			home.InitHomeArticleRouter(homeGroup)  // 文章查看的接口
			home.InitHomeCategoryRouter(homeGroup) // 分类相关的接口
		}
		adminGroup := public.Group("admin")
		userController := admin2.NewUserController()
		// 对后台接口进行鉴权控制
		adminGroup.POST("login", userController.Login)
		adminGroup.Use(middleware.Jwt())
		{
			admin.InitAdminFileRouter(adminGroup)
			admin.InitArticleFileRouter(adminGroup)
			admin.InitAdminCategoryRouter(adminGroup)
		}
	}

	return engine
}
