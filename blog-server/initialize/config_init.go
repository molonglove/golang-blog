package initialize

import (
	"blog-server/config"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

func InitDevConfig() *config.SystemConfig {
	return InitProvConfig("config/config.yml")
}

func InitProvConfig(path string) *config.SystemConfig {
	r := &config.SystemConfig{}
	file, err := ioutil.ReadFile(path)
	if err != nil {
		logrus.Fatalf("read config.yml error: %s\n", err.Error())
		panic(err.Error())
	}
	if yaml.Unmarshal(file, r) != nil {
		logrus.Fatalf("analysis config.yml error: %s\n", err)
		panic(err.Error())
	}
	return r
}
