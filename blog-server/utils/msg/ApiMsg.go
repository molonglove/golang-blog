package msg

const (
	SUCCESS string = "001001200"
	FAILED  string = "001001400"
)

var CodeMsg = map[string]string{
	SUCCESS: "操作成功",
	FAILED:  "操作失败",
}

// 消息实体
type ApiMsg struct {
	// code
	Code string `json:"code"`
	// msg
	Msg string `json:"msg"`
	// data
	Data interface{} `json:"data"`
}

func NewMsg() *ApiMsg {
	return new(ApiMsg)
}

// 成功
func (msg *ApiMsg) Success(data interface{}) *ApiMsg {
	msg.Code = SUCCESS
	msg.Msg = CodeMsg[SUCCESS]
	msg.Data = data
	return msg
}

// 失败
func (msg *ApiMsg) Failed(detailMsg string) *ApiMsg {
	msg.Code = FAILED
	msg.Msg = CodeMsg[FAILED]
	msg.Data = detailMsg
	return msg
}
