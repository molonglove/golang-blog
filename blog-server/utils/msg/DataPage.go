package msg

type DataPage struct {
	Page      int         `json:"page"`       // 页码
	Size      int         `json:"size"`       // 页大小
	PageTotal int64         `json:"page_total"` // 总页数
	Total     int64         `json:"total"`      // 数据总数
	HasNext   bool        `json:"has_next"`   // 是否有下一页
	HasPerv   bool        `json:"has_perv"`   // 是否有上一页
	Data      interface{} `json:"data"`       // 数据
}

// NewDataPage 获取新的数据分子
func NewDataPage(data interface{}, count int64, page int, size int) *DataPage {
	result := new(DataPage)
	result.Data = data
	result.Total = count
	result.PageTotal = count / int64(size)
	if count % int64(size) != 0 {
		result.PageTotal += 1
	}
	result.Page = page
	result.Size = size
	if result.Page <= 1 {
		result.HasPerv = false
	} else {
		result.HasPerv = true
	}

	if int64(result.Page) >= result.PageTotal {
		result.HasNext = false
	} else {
		result.HasNext = true
	}
	return result
}
