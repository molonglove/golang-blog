package models

import (
	"time"
)

// User 实体对应的数据表
type User struct {
	Id         int64     `xorm:"integer notnull autoincr pk comment('主键')"`  // 主键
	Username   string    `xorm:"varchar(50) notnull unique comment('用户名')"`  // 用户名
	Password   string    `xorm:"varchar(255) notnull comment('密码')"`         // 密码
	Salt       string    `xorm:"varchar(6) notnull comment('盐值')"`           // 盐值
	UserLevel  int       `xorm:"integer notnull default(0) comment('用户水平')"` // 用户水平
	IsLock     bool      `xorm:"bit notnull default(false) comment('是否锁定')"` // 是否锁定
	Email      string    `xorm:"varchar(25) unique comment('邮箱')"`           // 邮箱
	Phone      string    `xorm:"varchar(11) unique comment('手机号')"`          // 手机号
	NewTime    time.Time `xorm:"datetime comment('最近登陆时间')"`                 // 最近登陆时间
	Brief      string    `xorm:"varchar(255) comment('备注')"`                 // 备注
	CanView    bool      `xorm:"bit notnull default(true)"`                  // 删除标识
	CreateTime time.Time `xorm:"datetime comment('创建时间')"`                   // 创建时间
	UpdateTime time.Time `xorm:"datetime comment('更新时间')"`                   // 更新时间
}
