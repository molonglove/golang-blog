package views

import "mime/multipart"

type FileUploadVo struct {
	FileName string                `form:"fileName"`                     // 文件名/目录名
	IsDir    bool                  `form:"isDir" validate:"required"`    // 是否目录
	ParentId string                `form:"parentId" validate:"required"` // 父路径
	Brief    string                `form:"brief"`                        // 备注
	File     *multipart.FileHeader `form:"file"`                         // 文件
}

type FileListVo struct {
	Name     string `json:"name"`     // 文件名
	Size     int    `json:"size"`     // 页大小
	Page     int    `json:"page"`     // 当前页
	ParentId string `json:"parentId"` // 父Id
}
