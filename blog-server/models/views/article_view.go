package views

import (
	"mime/multipart"
	"time"
)

// ArticleListVo 后台文章列表VO查询
type ArticleListVo struct {
	Page      int    `json:"page"`      // 页数
	Limit     int    `json:"limit"`     // 页大小
	Title     string `json:"title"`     // 文章标题
	StartTime string `json:"startTime"` // 开始时间
	EndTime   string `json:"endTime"`   // 结束时间
	Markdown  string `json:"markdown"`  // markdown
}

// HomeArticlePageVo 前台文章列表POST VO
type HomeArticlePageVo struct {
	Page       int    `json:"page"`       // 页数
	Size       int    `json:"size"`       // 页大小
	CategoryId int    `json:"categoryId"` // 分类ID
	Tags       string `json:"tags"`       // 标签
	Time       string `json:"time"`       // 时间
}

type ArticleHomeRespVo struct {
	Id           int            `json:"id"` // 主键
	Title        string         `json:"title"`
	Synopsis     string         `json:"synopsis"`
	CreateTime   time.Time      `json:"createTime"`
	ArtLength    int            `json:"artLength"`
	IntegralNum  int            `json:"integralNum"`
	ReadsNum     int            `json:"readsNum"`
	ReviewsNum   int            `json:"reviewsNum"`
	ArtOverhead  bool           `json:"artOverhead"`
	PraiseNum    int            `json:"praiseNum"`
	Tags         string         `json:"tags"`
	CategoryList []CategoryList `json:"categoryList"`
	CateList     string         `json:"-"`
}

// AdminArticleAddVo 文章增加(form表单在网页进行编辑)Vo     "tui-editor": "^1.4.10",
type AdminArticleAddFormVo struct {
	Title       string `form:"title"`        // 文章标题
	Synopsis    string `form:"synopsis"`     // 备注
	Tags        string `form:"tags"`         // 标签
	CategoryId  string `form:"category_id"`  // 分类Id
	Markdown    string `form:"markdown"`     // 文章的md数据
	ArtDraft    bool   `form:"art_draft"`    // 时候为草稿
	ArtOverhead bool   `form:"art_overhead"` // 时候顶置
}

type AdminArticleList struct {
	Id          int       `json:"id"`
	Title       string    `json:"title"`
	Synopsis    string    `json:"synopsis"`
	CreateTime  time.Time `json:"createTime"`
	ArtOverhead bool      `json:"artOverhead"`
	ArtDraft    bool      `json:"artDraft"` // 时候为草稿
	Tags        string    `json:"tags"`
	CateList    string    `json:"cateList"`
	Markdown    string    `json:"markdown"` // markdown
}

// AdminArticleUploadVo 文章增加(通过文件上传)
type AdminArticleUploadVo struct {
	Title       string                `form:"title"`      // 文章标题
	Synopsis    string                `form:"synopsis"`   // 备注
	Tags        string                `form:"tags"`       // 标签
	CategoryId  int                   `form:"categoryId"` // 分类Id
	File        *multipart.FileHeader `form:"file"`       // 上传的文件
	ArtDraft    bool                  `form:"isDraft"`    // 时候为草稿
	ArtOverhead bool                  `form:"isOverhead"` // 时候顶置
}

type ArticleDetailVo struct {
	Id           string         `json:"id"`          // 主键
	Title        string         `json:"title"`       // 文章标题
	Synopsis     string         `json:"synopsis"`    // 文章备注
	Markdown     string         `json:"markdown"`    // markdown
	CreateTime   time.Time      `json:"createTime"`  // 创建时间
	ArtLength    int            `json:"artLength"`   // 文章长度
	IntegralNum  int            `json:"integralNum"` //
	ReadsNum     int            `json:"readsNum"`
	ReviewsNum   int            `json:"reviewsNum"`
	PraiseNum    int            `json:"praiseNum"`
	Tags         []string       `json:"tags"`
	CategoryList []CategoryList `json:"categoryList"` //

}

type CategoryList struct {
	Id    int    `json:"id"`    // 分类ID
	Title string `json:"title"` // 分类名称
}

// ArticleArchiveVo 归档数据实体
type ArticleArchiveVo struct {
	Year  string          `json:"year"`  // 时间（年）
	Total int64           `json:"total"` // 总数据量
	List  []ArchiveItemVo `json:"list"`  // 具体数据
}

// ArchiveItemVo 归档数据的Item
type ArchiveItemVo struct {
	CreateTime string `json:"createTime"`
	Title      string `json:"title"`
	Synopsis   string `json:"synopsis"`
	Id         int    `json:"id"`
}
