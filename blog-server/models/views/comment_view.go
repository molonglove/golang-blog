package views

import "time"

// AdminCommentRespVo 后台查询所有评论的查询VO
type AdminCommentRespVo struct {
	Page  int    `json:"page"`  // 当前页
	Size  int    `json:"size"`  // 页大小
	Start string `json:"start"` // 开始时间
	End   string `json:"end"`   // 结束时间
}

// HomeCommentArtVo 文章评论信息实体
type HomeCommentArtVo struct {
	Id         int64              `json:"id"`
	ArtId      int64              `json:"artId"`
	Content    string             `json:"content"`
	CreateTime time.Time          `json:"createTime"`
	Children   []HomeCommentArtVo `json:"children"`
}
