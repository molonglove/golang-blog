package views

// CategoryPagesVo 分类分页查询
type CategoryPagesVo struct {
	Page  int    `json:"page"`  // 页码
	Size  int    `json:"size"`  // 页大小
	Title string `json:"title"` // 分类名称
}

// CategoryAddVo 分类增加VO
type CategoryAddVo struct {
	Title string `json:"title"` //标题
	Brief string `json:"brief"` // 备注
}

// CategoryUpdateVo 分类修改VO
type CategoryUpdateVo struct {
	Id    int    `json:"id"`    // 主键
	Title string `json:"title"` //标题
	Brief string `json:"brief"` // 备注
}
