package views

// LoginVo 登录实体
type LoginVo struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// LoginResultVo 登录返回数据
type LoginResultVo struct {
	Id       int64  `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Phone    string `json:"phone"`
	Brief    string `json:"brief"`
	Token    string `json:"token"`
}
