package models

import "time"

type Article struct {
	Id          int       `xorm:"pk autoincr"` // 主键
	Title       string    `xorm:"varchar(50) unique notnull comment('文章标题')" json:"title"`
	Synopsis    string    `xorm:"varchar(255) notnull comment('文章备注')" json:"synopsis"`
	Detail      string    `xorm:"text null comment('html内容')" json:"detail"`
	Markdown    string    `xorm:"text not null comment('md内容')" json:"markdown"`
	CreateTime  time.Time `xorm:"comment('创建时间') datetime" json:"create_time"`
	UpdateTime  time.Time `xorm:"comment('更新时间') datetime" json:"update_time"`
	CanView     bool      `xorm:"bit notnull default(true) comment('是否删除')" json:"can_view"`
	ArtDraft    bool      `xorm:"bit notnull default(false) comment('是否为草稿')" json:"art_draft"`
	ArtLength   int       `xorm:"integer default(0) comment('文章长度')" json:"art_length"`
	IntegralNum int       `xorm:"integer default(0) comment('阅读积分')" json:"integral_num"`
	ReadsNum    int       `xorm:"integer default(0) comment('阅读数'')" json:"reads_num"`
	ReviewsNum  int       `xorm:"integer default(0) comment('评论数')" json:"reviews_num"`
	ArtOverhead bool      `xorm:"bit notnull default(false) comment('是否顶置')" json:"art_overhead"`
	PraiseNum   int       `xorm:"integer default(0) comment('点赞数')" json:"praise_num"`
	Tags        string    `xorm:"varchar(255) notnull comment('文章标签')" json:"tags"`
}
