package models

import "time"

// Comment 评论表
type Comment struct {
	Id         int64     `xorm:"integer notnull autoincr pk comment('主键')" json:"id"`
	ArtId      int64     `xorm:"integer notnull comment('文章Id')" json:"artId"`             // 文章ID
	UserId     string    `xorm:"varchar(50) notnull comment('发布评论的信息，邮箱')" json:"userId"`  // 标识发表评论人的联系地址
	Pass       bool      `xorm:"bit notnull default(pass) comment('是否通过审核')" json:"pass"`  // 是否存在
	Content    string    `xorm:"varchar(255) notnull comment('评论内容')" json:"content"`      // 评论内容
	CreateTime time.Time `xorm:"datetime comment('创建时间')"`                                 // 创建时间
	UpdateTime time.Time `xorm:"datetime comment('更新时间')"`                                 // 更新时间
	CanView    bool      `xorm:"bit notnull default(true) comment('是否删除')" json:"canView"` // 是否删除
}
