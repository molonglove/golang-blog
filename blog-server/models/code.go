package models

import "time"

// Code 代码库
type Code struct {
	Id         int       `xorm:"integer notnull autoincr pk comment('主键')" json:"id"`
	Name       string    `xorm:"varchar(50) unique notnull comment('代码主题')" json:"name"`
	Content    string    `xorm:"text notnull comment('代码内容')" json:"content"`
	Brief      string    `xorm:"varchar(150) unique notnull comment('代码备注')" json:"brief"` // 代码备注
	CreateTime time.Time `xorm:"datetime comment('创建时间')" json:"create_time"`              // 创建时间
	UpdateTime time.Time `xorm:"datetime comment('更新时间')" json:"update_time"`              // 更新时间
	CanView    bool      `xorm:"bit notnull default(true) comment('是否删除')" json:"can_view"`
}
