package models

import "time"

// CodeType 代码类型
type CodeType struct {
	Id         int       `xorm:"integer notnull autoincr pk comment('主键')" json:"id"`
	TypeName   string    `xorm:"varchar(50) unique notnull comment('代码主题')" json:"typeName"` // 类型名称
	CreateTime time.Time `xorm:"datetime comment('创建时间')"`                                   // 创建时间
	UpdateTime time.Time `xorm:"datetime comment('更新时间')"`                                   // 更新时间
	CanView    bool      `xorm:"bit notnull default(true) comment('是否删除')" json:"canView"`   // 状态
}
