package models

// ArtCate 分类和文章ID
type ArtCate struct {
	Id     int `xorm:"integer notnull pk autoincr comment('主键')" json:"id"` // 主键
	ArtId  int `xorm:"integer notnull comment('文章ID')" json:"art_id"`       // 文章主键
	CateId int `xorm:"integer notnull comment('分类ID')" json:"cate_id"`      // 分类主键
}
