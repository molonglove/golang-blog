package models

// CodeMiddle 代码库和代码分类的中间表
type CodeMiddle struct {
	Id     int `xorm:"integer notnull autoincr pk comment('主键')" json:"id"`
	CodeId int `xorm:"integer notnull comment('代码库主键')" json:"codeId"`
	TypeId int `xorm:"integer notnull comment('代码分类主键')" json:"typeId"`
}
