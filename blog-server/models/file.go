package models

import (
	"time"
)

type FileSync struct {
	Id         int       `xorm:"integer notnull pk autoincr comment('主键')" json:"id"`  // 主键
	FileName   string    `xorm:"varchar(255) notnull comment('文件名称')"`                 // 文件名称
	FilePath   string    `xorm:"varchar(255) notnull comment(文件路径)"`                   // 文件路径
	FileType   string    `xorm:"varchar(20) notnull comment('文件类型')"`                  // 文件类型
	FileSize   int64     `xorm:"int default(0) comment('文件大小')"`                       // 文件大小
	FileOrDir  int       `xorm:"bit notnull default(true) comment('文件true或者目录false')"` // 时候目录
	ParentId   string    `xorm:"varchar(64) notnull default('root')"`                  // 父路径
	CanView    bool      `xorm:"bit notnull default(true) comment('是否删除')" json:"canView"`
	Brief      string    `xorm:"varchar(255) comment('备注')"` // 文件备注
	UploadTime time.Time `xorm:"comment('上传时间') uploadTime" json:"uploadTime"`
}

// NewFileSync 文件
func NewFileSync(fileName string, filePath string, fileType string, fileSize int64, parentId string, Brief string) *FileSync {
	return &FileSync{
		CanView:    true,
		FileOrDir:  1,
		UploadTime: time.Now(),
		FileName:   fileName,
		FilePath:   filePath,
		FileType:   fileType,
		FileSize:   fileSize,
		ParentId:   parentId,
		Brief:      Brief,
	}
}

// NewDirFileSync 文件目录
func NewDirFileSync(fileName string, parentId string, Brief string) *FileSync {
	return &FileSync{
		CanView:    true,
		FileOrDir:  0,
		UploadTime: time.Now(),
		FileName:   fileName,
		ParentId:   parentId,
		Brief:      Brief,
		FileType:   "dir",
	}
}
