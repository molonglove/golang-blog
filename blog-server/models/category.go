package models

import "time"

// Category 分类表
type Category struct {
	Id         int       `xorm:"integer notnull autoincr pk comment('主键')" json:"id"`      // 主键
	Title      string    `xorm:"varchar(50) unique notnull comment('分类标题')" json:"title"`  // 标题
	Brief      string    `xorm:"varchar(150) unique notnull comment('分类备注')" json:"brief"` // 标题
	Icon       string    `xorm:"varchar(50) unique null comment('分类图标')" json:"icon"`      // 分类图标
	CreateTime time.Time `xorm:"datetime comment('创建时间')" json:"create_time"`              // 创建时间
	UpdateTime time.Time `xorm:"datetime comment('更新时间')" json:"update_time"`              // 更新时间
	CanView    bool      `xorm:"bit notnull default(true) comment('是否删除')" json:"can_view"`
	SortNum    int       `xorm:"integer default(0) comment('文章数量')" json:"sort_num"`
}
