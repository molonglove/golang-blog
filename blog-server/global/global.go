package global

import (
	"blog-server/config"
	"xorm.io/xorm"
)

var (
	Config *config.SystemConfig
	Engine *xorm.Engine
)
