# golang-blog博客网站

#### 项目介绍
基于golang+React的博客网站，支持常规的博客文章管理的功能，并包含一些有意思的功能：代码库、图库，文件库等等。

首页大体如下：

![](https://molongyin.oss-cn-beijing.aliyuncs.com/blog_images/blog_home_index.png)

#### 软件架构

整体使用前后端分离开发

- 服务端：gin、xorm、jwt、logrus
- web端：react、antd、react-redux、react-router、axios

现阶段项目还在开发中，如有使用其他的框架，在添加！

#### 模块介绍

##### 前端已经完成页面：

文章详情：

![](https://molongyin.oss-cn-beijing.aliyuncs.com/blog_images/blog_home_article_detail.png)

文章归档：

![](https://molongyin.oss-cn-beijing.aliyuncs.com/blog_images/blog_home_arctive.png)

##### 后端已完成的页面：

登录页面：

![](https://molongyin.oss-cn-beijing.aliyuncs.com/blog_images/blog_login_page.png)

文章分类管理：

![](https://molongyin.oss-cn-beijing.aliyuncs.com/blog_images/blog_admin_category.png)

文章管理：

![](https://molongyin.oss-cn-beijing.aliyuncs.com/blog_images/blog_admin_article.png)

其他模块正在开发，敬请期待......

#### 安装教程

##### 服务段：

先编译go文件：

```shell
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build main.go   # mac的编译
========================================================================
set GOARCH=amd64
set GOOS=linux
go build   # win的编译
```

接着我们可以将`blog-server/config/config.yml`文件复制一份修改：

```yaml
# 数据库配置
mysql:
  category: mysql
  prefix: blog_   
  database: blog_db_new   # 修改你自己的数据库名，ip和端口账号密码
  ip: 127.0.0.1      
  part: 3306
  username: root
  password: 123456
  print: true
  max-idle-conns: 10
  max-open-conns: 100
# 服务器端口
system:
  part: :9091
  stauts: debug
logger:
  path: /home/blog_server/logs
  name: memory_box.log
# jwt配置
jwt:
  sign-key: 123456
  expires-time: 3000
  issuer: molong
# 验证码
captcha:
  key-long: 6
  img-width: 240
  img-height: 60
file:
  basePath: /home/blog_server/files
```

接这样`config.yaml`和`go build`之后的二进制文件复制到云服务器上

![](https://molongyin.oss-cn-beijing.aliyuncs.com/blog_images/blog_file_list.png)

需要创建`files`、`logs`文件夹。启动脚本如下（需要给脚本添加执行权限）：

```shell
nohup ./blog_server --config=config.yml > run.out 2>&1 &
```

##### 前端：

修改`blog_web/src/api/http.js`文件中的服务端接口路径：

```js
axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? 'http://云服务器IP地址:8080/api' : 'http://localhost:9091/api';
```

点击`package.json`中的`build`命令:

![](https://molongyin.oss-cn-beijing.aliyuncs.com/blog_images/blog_web_package.png)

接着将`build`文件夹下的文件全部复制到云服务器的指定文件夹下。

##### nginx配置

```protobuf
server {
        listen       80;
        server_name  localhost;

        gzip on;

        location / {
                root /opt/nginx/html/blog/;  # 你放置打包文件的目录
                index index.html;
                try_files $uri $uri/ @router;
                index index.html;
        }
        location @router {
                rewrite ^.*$ /index.html last;
        }

        location /api {
                proxy_pass       http://IP:8080/api;
        }
}
```

配置好之后重新加载配置文件就可以。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
