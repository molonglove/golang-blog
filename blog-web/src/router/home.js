import HomeIndex from "../pages/home/index";
import HomeSpecial from "../pages/home/special";
import HomeArticle from "../pages/home/article";
import HomeCode from "../pages/home/code";
import HomeImages from "../pages/home/images";
import HomeAbout from "../pages/home/about";
import HomeArchive from "../pages/home/archive";
import HomeCategory from "../pages/home/category";

export const HomeRoutes = [
    {
        path: '/home/index',
        title: '博客首页',
        component: HomeIndex,
        exact: true,
        children: []
    },
    {
        path: '/home/special',
        title: '专栏',
        component: HomeSpecial,
        exact: true,
        children: []
    },
	{
		path: '/home/category',
		title: '分类',
		component: HomeCategory,
		exact: true,
		children: []
	},
    {
        path: '/home/article/:id',
        title: '文章',
        component: HomeArticle,
        exact: true,
        children: []
    },
	{
		path: '/home/codes',
		title: '代码库',
		component: HomeCode,
		exact: true,
		children: []
	},
	{
		path: '/home/images',
		title: '图库',
		component: HomeImages,
		exact: true,
		children: []
	},
	{
		path: '/home/about',
		title: '关于',
		component: HomeAbout,
		exact: true,
		children: []
	},
	{
		path: '/home/archive',
		title: '归档',
		component: HomeArchive,
		exact: true,
		children: []
	}
]