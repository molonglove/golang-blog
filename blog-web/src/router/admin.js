import ImagePool from "../pages/admin/pool";
import AdminArticle from "../pages/admin/article";
import AdminPanel from "../pages/admin/panel";
import AdminSystem from "../pages/admin/system";
import AdminCategory from "../pages/admin/category";
import AdminFile from "../pages/admin/file";
import AdminComment from "../pages/admin/comment";


export const AdminRoutes = [
    {
        path: '/admin/panel',
        title: '网站数据',
        component: AdminPanel,
        icon: 'icon-tongji1',
        exact: true,
        children: []
    },
    {
        path: '/admin/article',
        title: '文章管理',
        component: AdminArticle,
        icon: 'icon-svgwrite',
        exact: true,
        children: []
    },
    {
        path: '/admin/comment',
        title: '评论管理',
        component: AdminComment,
        icon: 'icon-pinglun',
        exact: true,
        children: []
    },
    {
        path: '/admin/category',
        title: '分类管理',
        component: AdminCategory,
        icon: 'icon-fenlei',
        exact: true,
        children: []
    },
    {
        path: '/admin/file',
        title: '文件仓库',
        component: AdminFile,
        icon: 'icon-wenjian',
        exact: true,
        children: []
    },
    {
        path: '/admin/pool',
        title: '图库资源',
        component: ImagePool,
        icon: 'icon-tuku',
        exact: true,
        children: []
    },
    {
        path: '/admin/system',
        title: '网站设置',
        component: AdminSystem,
        icon: 'icon-shezhi-tianchong',
        exact: true,
        children: []
    },

]


