import {Get, Post, Delete} from '../http';

// 删除分类
export const DeleteCategory = (id) => {
    return new Promise((resolve, reject) => {
        Delete('/admin/category/delete', {id: id}).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err)
        })
    })
}


/**
 * 获取所有的分类
 * @param data
 * @returns {Promise<unknown>}
 * @constructor
 */
export const CategoryAll = () => {
    return new Promise((resolve, reject) => {
        Get('/admin/category/all').then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}


/**
 * 分页获取分类信息
 * @param data
 * @returns {Promise<unknown>}
 * @constructor
 */
export const PageCategory = (data) => {
    return new Promise((resolve, reject) => {
        Post('/admin/category/pages', data).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err)
        })
    })
}


/**
 * 增加分类信息
 * @param data
 * @returns {Promise<unknown>}
 * @constructor
 */
export const CategoryAdd = (data) => {
    return new Promise((resolve, reject) => {
        Post('/admin/category/add', data).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err)
        })
    })
}

/**
 * 删除数据
 * @param data
 * @returns {Promise<unknown>}
 * @constructor
 */
export const CategoryDel = (data) => {
    return new Promise((resolve, reject) => {
        Get('/admin/category/delete', data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}


