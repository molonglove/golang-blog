// 文章资源列表

import {Get, Post, Delete} from '../http';

/**
 * 获取某一个文章信息
 * @param params
 * @constructor
 */
export const TestUserStatus = (params) => {
  return new Promise((resolve, reject) => {
    Get('/user/status').then(res => {
      resolve(res.data)
    }, err => {
      reject(err)
    })
  })
};

/**
 * 删除文章
 * @param ids
 * @returns {Promise<unknown>}
 * @constructor
 */
export const DelArticle = (params) => {
    return new Promise((resolve, reject) => {
        Delete('/admin/article/delete', params).then(res => {
            resolve(res.data)
        }, err => {
            reject(err)
        })
    })
}


/**
 * 获取文章的内容
 * @param params
 * @constructor
 */
export const GetArticleHtml = (params) => {
    return new Promise((resolve, reject) => {
        Get('/admin/article/content?id=' + params).then(res => {
            resolve(res.data)
        }, err => {
            reject(err)
        })
    })
}


/**
 * 文章的分页信息
 * @param params
 * @returns {Promise<unknown>}
 * @constructor
 */
export const ArticlePages = (params) => {
  return new Promise(((resolve, reject) => {
    Post('/admin/article/list', params).then(res => {
      resolve(res.data)
    }, err => {
      reject(err)
    })
  }))
}


/**
 * 文章上传
 * @param params
 * @constructor
 */
export const ArticleUpload = (params) => {
	return new Promise(((resolve, reject) => {
		Post('/admin/article/upload', params).then(res => {
			resolve(res.data)
		}, err => {
			reject(err)
		})
	}))
}

