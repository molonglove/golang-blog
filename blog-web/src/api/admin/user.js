import {Post} from '../http';


/**
 * 登录
 * @param params
 * @returns {Promise<unknown>}
 * @constructor
 */
export const Login = (params) => {
    return new Promise((resolve, reject) => {
        Post('/admin/login', params).then(res => {
            resolve(res.data)
        }, err => {
            reject(err)
        })
    })
}