import axios from "axios";
import {store} from "../redux/store";


// axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? 'http://192.168.231.131:8080/api' : 'http://localhost:9091/api';
axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? 'http://192.168.4.72:9091/api' : 'http://localhost:9091/api';
// axios.defaults.headers['Content-Type'] = 'application/json;charset=UTF-8';
axios.defaults.withCredentials = true;


export const Get = (url, data) => {
    return new Promise(((resolve, reject) => {
        axios.get(url, {params: data}).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err)
        })
    }))
}

export const Post = (url, data) => axios.post(url, data)


export const Delete = (url, data) => axios.delete(url, {params: data})


axios.interceptors.request.use(
    config => {
        console.log("前置拦截器", config, store.getState().user)
        if (config.url.match('^/admin/') !== null || config.url !== '/admin/login') {
            const {token} = store.getState().user
            config.headers['Authorization'] = "Blog " + token;
        }
        return config
    },
    error => {

    }
)
