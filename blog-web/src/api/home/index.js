import {Get, Post, Delete} from '../http';


/**
 * 获取文章列表
 * @param params
 * @returns {Promise<unknown>}
 * @constructor
 */
export const ArticleSimpleList = (params) => {
    return new Promise((resolve, reject) => {
        Post('/home/article/page', params).then(res => {
            console.log('===============================')
            console.log(res)
            console.log('===============================')
            resolve(res.data)
        }, err => {
            reject(err)
        })
    })
}


/**
 * 获取某一个文章信息
 * @param params
 * @constructor
 */
export const ArticleOneDetail = (params) => {
    return new Promise((resolve, reject) => {
        Get('/home/article/detail', params).then(res => {
            resolve(res)
        }, err => {
            reject(err)
        })
    })
};


/**
 * 获取所有的分类信息
 * @returns {Promise<unknown>}
 * @constructor
 */
export const CategoryList = () => {
    return new Promise(((resolve, reject) => {
        Get('/home/category/all').then(res => {
            resolve(res.data)
        }, err => {
            reject(err)
        })
    }))
}


/**
 * 获取文章的所有的标签页
 * @returns {Promise<unknown>}
 * @constructor
 */
export const ArticleTags = () => {
	return new Promise(((resolve, reject) => {
		Get('/home/article/tags').then(res => {
			resolve(res.data);
		}, err => {
			reject(err);
		})
	}))
}

/**
 * 获取文章归档信息
 * @param year
 * @returns {Promise<unknown>}
 * @constructor
 */
export const ArticleArchive = (year) => {
    return new Promise((resolve, reject) => {
        Get('/home/article/archive?year=' + year).then(res => {
            resolve(res.data);
        }, err => {
            reject(err);
        })
    })
}

