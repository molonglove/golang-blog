import {ADMIN_LOGIN_USER_INFO} from "../constant";

const userInitData = {
    id: '',
    username: '',
    brief: '',
    token: '',
    status: false
}

export default function userReducer(state = userInitData, action) {
    const {type, data} = action
    switch (type) {
        case ADMIN_LOGIN_USER_INFO:
            return {
                ...state,
                ...data
            }
        default:
            return state
    }
}



