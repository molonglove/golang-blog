import {combineReducers, createStore} from 'redux'
import {persistStore, persistReducer} from 'redux-persist';
import {composeWithDevTools} from "redux-devtools-extension";
import storage from 'redux-persist/lib/storage';
import articleReducer from "./models/article";
import userReducer from "./models/user";

const allReducer = combineReducers({
    article: articleReducer,
    user: userReducer
})

const persistConfig = {
    key: 'root',
    storage: storage
};

const persistedReducer  = persistReducer(persistConfig, allReducer)

export const store = createStore(persistedReducer, composeWithDevTools())

export const persist = persistStore(store)
