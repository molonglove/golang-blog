import {
	HOME_ARTICLE_PARAMS,
	HOME_ARTICLE_CATEGORY,
	HOME_ARTICLE_DEL_PARAMS
} from '../constant';


// 设置文章分类ID、
export const setArticleParams = data => ({type: HOME_ARTICLE_PARAMS, data})

// 删除属性
export const delArticleParams = data => ({type: HOME_ARTICLE_DEL_PARAMS, data})

// 获取文章分类
export const setArticleCate = data => ({type: HOME_ARTICLE_CATEGORY, data})
