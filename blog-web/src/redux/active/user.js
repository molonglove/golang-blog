import {ADMIN_LOGIN_USER_INFO} from "../constant";

// 设置登录用户的信息
export const setUserInfo = data => ({type: ADMIN_LOGIN_USER_INFO, data})