const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = function (app) {
    console.log(app)
    app.use (
        createProxyMiddleware ('/api',{
            target: 'http://localhost:9091/',
            secure: false,
            changeOrigin: true,
            ws: true,
            pathRewrite: {
                '^/api': '/api'
            }
        })
    )

};