import React from "react";
import './index.less';
import Img from './1.jpg';


class HomeSpecial extends React.Component {

    state = {
        list: [
            {
                title: '32堂微服务架构设计与落地精讲课',
                brief: '跟着 10 年资深架构师一起学微服务架构',
                total: 12,
                readNum: 300,
                list: [
                    '开篇词-什么是微服务，是否要实施微服务？',
                    '单体服务转为微服务体系需要注意什么问题？',
                    '如何进行微服务的技术选型？',
                    '开启微服务，我们需要配齐多少设施？'
                ]
            },
            {
                title: 'Java 并发编程深度解析',
                brief: '技巧总结+原理剖析，面试工作更通透',
                total: 54,
                readNum: 540,
                list: [
                    '开篇词：为什么Java并发如此重要',
                    '多线程带来哪些问题'
                ]
            }
        ],
        page: 1,
        size: 10,
        total: 0,
    }


    render() {

        const { list } = this.state

        return (
            <div className="special-area">
                {
                    list.map((item, index) => {
                        return (
                            <div key={index} className="special-item">
                                <div className="item-left-img">
                                    <img src={Img} alt="" />
                                </div>
                                <div className="item-right-main">
                                    <div className="right-main-title">{item.title}</div>
                                    <div className="right-main-brief">
                                        <div>{item.brief}</div>
                                        <div> - </div>
                                        <div>共{item.total}节</div>
                                    </div>
                                    <div className="right-main-list">
                                        {
                                            item.list.map((innerItem, idx) => {
                                                return (
                                                    <div className="list-item" key={idx}>{idx + 1}. {innerItem}</div>
                                                )
                                            })
                                        }
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }

            </div>

        )
    }
}


export default HomeSpecial