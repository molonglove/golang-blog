import React from "react";
import BackGroup from '../../../assets/images/kai.jpg'
import IconFont from "../../../commons/CommonIcon";
import {ArticleSimpleList} from "../../../api/home";
import loading from "../../../commons/loading";
import './index.less';
import {timeFormat} from "../../../utils/timeUtils";
import {connect} from "react-redux";
import {
	setArticleParams,
	delArticleParams
} from "../../../redux/active/article";

@connect(
    state => state.article,
	{
		setArticleParams,
		delArticleParams
	}
)
class HomeIndex extends React.Component{
    state = {
        // 返回的数据
        article: {
            data: [],
            total: 0,
            pages: 0
        },
        closeStyle: {
            position: 'absolute',
            top: -8 + 'px',
            right: -8 + 'px',
        }
    }

    componentDidMount() {
        this.loadArticleList({...this.props.homeSearch});
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("prevProps", prevProps)
        if (JSON.stringify(prevProps.homeSearch) !== JSON.stringify(this.props.homeSearch)) {
            this.loadArticleList({...this.props.homeSearch});
        }
    }




    /**
     * 加载文章列表
     */
    loadArticleList = (params) => {
        const mask = loading();
        // debugger;
        ArticleSimpleList(params).then(res => {
            mask.close()
	        console.log(res);
            if (res.code === "001001200") {
            	const temp = res.data;
	            this.setState({article: {
			            data: temp.data,
			            total: temp.total,
			            pages: temp.pageTotal
		            }})
            }
	        

        }, err => {
            mask.close()
            console.log(err)
        })
    }

    // 时间点击
    timeClickHandler = (time, e) => {
	    this.props.setArticleParams({value: timeFormat(time), field: 'time'})
    }

    // 分类点击
    categoryClickHandler = (e, cateId) => {
	    this.props.setArticleParams({value: cateId, field: 'categoryId'})
    }

    // 标签点击
    tagsClickHandler = (tags, e) => {
	    this.props.setArticleParams({value: tags, field: 'tags'})
    }

    // 清空条件
    cleanSelectCondition = (e, field) => {
    	// debugger
    	this.props.delArticleParams({field})
    }

    render() {
        const {article} = this.state;
        const {homeSearch} = this.props;

        return (
            <div className="blog_index">
                <div className="top_index" style={{backgroundImage: `url(${BackGroup})`}}>
                    <div className="top_mask">
                        <h1>一个人可以被毁灭，但绝不可以被打败</h1>
                        <h2> —— 凯</h2>
                    </div>
                </div>
                {/* 条件搜索展示区 */}
                {
                    (homeSearch.categoryName !== '' || homeSearch.tags !== '' || homeSearch.time !== '') ?
                        (
                            <div className="select_article_area">
                                <div className="select_title">搜索条件：</div>
                                <div className="select_field">
                                    {homeSearch.categoryName !== '' ? <div className="field_item">{homeSearch.categoryName}<IconFont type="icon-guanbi" style={this.state.closeStyle} onClick={(e) => this.cleanSelectCondition(e, 'categoryId')} /></div> : null}
                                    {homeSearch.tags !== '' ? <div className="field_item">{homeSearch.tags}<IconFont type="icon-guanbi" style={this.state.closeStyle} onClick={(e) => this.cleanSelectCondition(e, 'tags')} /></div> : null}
                                    {homeSearch.time !== '' ? <div className="field_item">{homeSearch.time}<IconFont type="icon-guanbi" style={this.state.closeStyle} onClick={(e) => this.cleanSelectCondition(e, 'time')} /></div> : null}
                                </div>
                            </div>
                        ) : null
                }
                <div className="article_area">
	                {
		                article.data.map(item => {
			                let top;
			                if (item.isOverhead) {
				                top = (
					                <div className="top_icon">
						                <IconFont type="icon-dingzhi" style={{fontSize: 3 + 'rem'}} />
					                </div>
				                )
			                }
			                return (
				                <div className="article_item" key={item.id}>
					                {top}
					                <div className="item_title"
					                     onClick={() => this.props.history.push({pathname: '/home/article/' + item.id})}
					                >{item.title}</div>
					                <div className="item_field">
						                <div className="field_item item_with" onClick={(e) => this.timeClickHandler(item.createTime, e)}>
							                <IconFont type="icon-riqi" style={{marginRight: 5 + 'px', fontSize: 19 + 'px'}} />
							                {timeFormat(item.createTime)}
						                </div>
						                <div className="field_item item_with">
							                <IconFont type="icon-leibie" style={{marginRight: 5 + 'px'}} />
							                {
								                item.categoryList.map((innerItem, index) => {
									                if (item.categoryList.length - 1 === index) {
										                return <span key={index} onClick={(e) => this.categoryClickHandler(e, item.id)}>{innerItem.title}</span>
									                } else {
										                return <span key={index} onClick={(e) => this.categoryClickHandler(e, item.id)}>{innerItem.title}</span>
									                }
								                })
							                }
							                {item.categoryName}
						                </div>
						                <div className="field_item">
							                <IconFont type="icon-biaoqian" style={{marginRight: 5 + 'px'}} />
							                {
								                item.tags.trim().split(",").map((innerItem, index) => {
									                return (<span key={index} onClick={(e) => this.tagsClickHandler(innerItem, e)}>{innerItem}</span>)
								                })
							                }
						                </div>
					                </div>
					                <div className="item_brief">
						                {item.synopsis}
					                </div>
				                </div>
			                )
		                })
	                }
                </div>
            </div>
        )
    }
}

export default HomeIndex
