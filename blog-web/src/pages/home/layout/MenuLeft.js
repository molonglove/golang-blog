import * as React from "react";
import IconFont from "../../../commons/CommonIcon";


export default class MenuLeft extends React.Component {
	state = {
		menuUrl: [
			{
				id: 1,
				title: '首页',
				icon: 'icon-yemian-copy-copy',
				url: '/home/index',
				active: true,
			},
			{
				id: 2,
				title: '专栏',
				icon: 'icon-column',
				url: '/home/special',
				active: false,
			},
			{
				id: 3,
				title: '归档',
				icon: 'icon-placefileIcon',
				url: '/home/archive',
				active: false,
			},
			{
				id: 4,
				title: '代码库',
				icon: 'icon-daima1',
				url: '/home/codes',
				active: false,
			},
			{
				id: 5,
				title: '图库',
				icon: 'icon-tuku1',
				url: '/home/images',
				active: false,
			},
			{
				id: 6,
				title: '关于',
				icon: 'icon-guanyu',
				url: '/home/about',
				active: false,
			}
		]
	}
	
	handleMenuClick = (data) => {
		const {menuUrl} = this.state;
		let newMenuUrl = [...menuUrl];
		newMenuUrl.forEach(item => item.active = false)
		newMenuUrl.forEach(item => {
			if (item.id === data.id) {
				item.active = true;
			}
		})
		this.setState({menuUrl: newMenuUrl})
		this.props.pageJump(data.url)
	}

    render() {
        return (
            <div className="blog_main_left">
	            {/* 介绍 */}
                <div className="left_item_card item_card">
                    {/* 名字 */}
                    <div className="card_name">
                        <div className="card_name_content">星辰 ● 漫步者</div>
                    </div>
                    {/* 描述 */}
                    <div className="card_brief">
                        <span className="card_brief_main">简洁的代码简单直接。简洁的代码如同优美的散文。简洁的代码从不隐藏设计者的意图，充满了干净利落的抽象和直接了当的控制语句。</span>
                        <span className="card_brief_author"> ——— grady booch</span>
                    </div>
                    {/* 总数 */}
                    <div className="card_count">
                        <div className="card_count_item">
                            <IconFont type="icon-wenzhang" style={{fontSize: 20 + 'px', color: '#141414'}} />
                            <span>16</span>
                        </div>
                        <div className="card_count_item">
                            <IconFont type="icon-leibie" style={{fontSize: 20 + 'px'}} />
                            <span>32</span>
                        </div>
                        <div className="card_count_item">
                            <IconFont type="icon-biaoqian" style={{fontSize: 20 + 'px'}} />
                            <span>12</span>
                        </div>
                        <div className="card_count_item">
                            <IconFont type="icon-daima" style={{fontSize: 20 + 'px'}} />
                            <span>45</span>
                        </div>
                    </div>
                    {/* 图标 */}
                    <div className="card_icon">
                        <div className="icon_item"><IconFont style={{fontSize: 25 + 'px'}} type="icon-mayun" /></div>
                        <div className="icon_item"><IconFont style={{fontSize: 25 + 'px'}} type="icon-csdn" /></div>
                        <div className="icon_item"><IconFont style={{fontSize: 25 + 'px'}} type="icon-qq" /></div>
                        <div className="icon_item"><IconFont style={{fontSize: 25 + 'px', color: '#f5222d'}} type="icon-zu" /></div>
                    </div>
                    {/* 图片 */}
                    <div className="card_image">
                        <img src="https://molongyin.oss-cn-beijing.aliyuncs.com/blog_images/%E5%85%AC%E4%BC%97%E5%8F%B7.jpg" alt=""/>
                    </div>
                </div>
	            {/* 菜单 */}
                <div className={['left_item_card', 'card_menu', this.props.status ? 'card_pushpin' : null].join(' ')}>
                    <div className="card_menu_top">
                        <div className="menu_top_left">
                            <IconFont type="icon-menu" style={{marginRight: 5 + 'px'}}/>
                            菜单
                        </div>
                        <div className="menu_top_right"><IconFont type="icon-zuixiaohua" /></div>
                    </div>
                    <div className="card_menu_list">
	                    {this.state.menuUrl.map(item => {
	                    	return (
			                    <div
				                    onClick={() => this.handleMenuClick(item)}
				                    className={["menu_item", item.active ? "menu_item_active": null ].join(' ')}
				                    key={item.id}>
				                    <div className="menu_item_inner">
					                    <IconFont
						                    type={item.icon}
						                    style={{marginRight: 12 + 'px', color: (item.active) ? "#f0f0f0" : "#000000"}} />
					                    {item.title}
				                    </div>
			                    </div>
		                    )
	                    })}
                    </div>
                </div>
                <div className="left_item_card">

                </div>
            </div>
        )
    }

}