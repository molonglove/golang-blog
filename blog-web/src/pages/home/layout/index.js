import * as React from "react";
import {withRouter} from 'react-router-dom'
import './index.less'
import HomeHeader from "./HomeHeader";
import HomeFooter from "./HomeFooter";
import MenuLeft from "./MenuLeft";
import MenuRight from "./MenuRight";

class HomeLayout extends React.Component {
	
	state = {
		leftStatus: false,
		rightStatus: false,
		topStatus: false
	}
	
	componentDidMount() {
		window.addEventListener('scroll', this.handleScroll)
	}
	
	handleScroll = (event) => {
		console.log('可视区域高度:', document.documentElement.clientHeight)
		console.log('滚动条滚动高度:', document.documentElement.scrollTop)
		console.log('滚动内容高度:', document.documentElement.scrollHeight)
		let topHeight = document.documentElement.scrollTop;
		if (topHeight <= 0) {
			this.setState({topStatus: false})
		} else {
			this.setState({topStatus: true})
		}

		if (topHeight >= 600) {
			this.setState({leftStatus: true})
		} else {
			this.setState({leftStatus: false})
		}

	}
	
	// 页面跳转
	pageJumpHandler = (url) => {
		this.props.history.push(url)
	}
	
    render() {
        return (
            <div className="blog_root">
                <HomeHeader />
                <div className="blog_main">
                    <MenuLeft
	                    pageJump={this.pageJumpHandler}
	                    status={this.state.leftStatus}/>
                    <div className="blog_main_middle">
                        {this.props.children}
                    </div>
                    <MenuRight top={this.state.topStatus} />
                </div>
                <HomeFooter />
            </div>
        )
    }
}

export default withRouter(HomeLayout)