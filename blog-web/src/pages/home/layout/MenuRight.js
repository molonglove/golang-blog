import * as React from "react";
import Top from '../../../assets/images/top.png';
import IconFont from "../../../commons/CommonIcon";
import {CategoryList, ArticleTags} from "../../../api/home";
import { connect } from "react-redux";
import {
	setArticleParams,
	setArticleCate
} from "../../../redux/active/article";

@connect(
	state => state.article.homeSearch,
	{
		setArticleParams,
		setArticleCate
	}
)
class MenuRight extends React.Component {


	componentDidMount() {
		this.loadCategory()
		this.loadTags()
	}


	state = {
		category: [],
		tags: []
	}


	handlerTop = () => {
		document.documentElement.scrollTop = 0;
	}



	
	// 加载分类信息
	loadCategory = () => {
		CategoryList().then(res => {
			this.setState({category: res})
			this.props.setArticleCate(res)
		}, err => {
			console.log(err)
		})
	}

	// 设置文章分类ID
	setCateId = (event, data) => {
		this.props.setArticleParams({value: data, field: 'categoryId'})
	}

	setTags = (event, data) => {
		this.props.setArticleParams({value: data, field: 'tags'})
	}
	
	// 加载标签
	loadTags = () => {
		ArticleTags().then(res => {
			this.setState({tags: res})
		}, err => {
			console.log(err)
		});
	}

	
    render() {
        return (
            <div className="blog_main_right">
	            {this.props.top &&
                    <div className="return_top" onClick={this.handlerTop} >
                        <img src={Top} alt="返回顶部"/>
                    </div>
	            }
	            <div className="right_card_item category_card">
		            <div className="card_category_top">
			            <div className="top_left">
				            <IconFont type="icon-leibie" style={{marginRight: 5 + 'px'}} />
				            分类
			            </div>
			            <div className="top_right">
				            <IconFont type="icon-zuixiaohua" />
			            </div>
		            </div>
		            <div className="card_category_main">
						{
							this.state.category.map(item => {
								return (
									<div
										className="category_item"
										key={item.id}
										onClick={(event) => this.setCateId(event, item.id)}>
										<div className="item_inner">
											<div className="item_title">{item.title}</div>
											<div className="item_num">{item.sortNum}</div>
										</div>
									</div>
								)
							})
						}
		            </div>
	            </div>
	            <div className="right_card_item tags_card">
		            <div className="tags_card_top">
			            <div className="tags_top_left">
				            <IconFont type="icon-biaoqian" style={{marginRight: 5 + 'px'}} />
				            标签
			            </div>
			            <div className="tags_top_right">
				            <IconFont type="icon-zuixiaohua" />
			            </div>
		            </div>
		            <div className="tags_card_main">
			            {this.state.tags.map((item, index) => {
			            	return <span
								onClick={(event) => this.setTags(event, item)}
								key={index}
								className="tag_item"
							>{item}</span>
			            })}
		            </div>
	            </div>
            </div>
        )
    }

}

export default MenuRight

