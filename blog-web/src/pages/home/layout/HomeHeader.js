import * as React from "react";
import {createFromIconfontCN} from "@ant-design/icons";
const IconFont = createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_2357337_a9i1rzbuk5s.js',
});

export default class HomeHeader extends React.Component {


    render() {
        return (
            <div className="blog_header">
                <div className="header_left">
                    <IconFont className="inner_icon" type="icon-gengduoneirong" />
                </div>
                <div className="header_middle">
                    <span className="middle_title">星辰 ● 漫步者</span>
                    <span className="middle_btn">博客</span>
                    <span className="middle_brief"> —— 生活是一杯清水，你放一点糖它就甜；放一点盐它就咸！</span>
                </div>
                <div className="header_right">
                    <IconFont type="icon-sousuo" />
                </div>
            </div>
        )
    }

}