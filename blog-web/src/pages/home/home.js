import React from "react";
import HomeLayout from "./layout";
import {Route, Switch, Redirect} from "react-router-dom";
import {HomeRoutes} from "../../router/home";
import NotFount from "../404";


export default function HomeMain() {
    return (
        <HomeLayout>
            <Switch>
                {
                    HomeRoutes.map(item => {
                        return (
                            <Route
                                path={item.path}
                                key={item.path}
                                exact={item.exact}
                                render={
                                    routeProps => {
                                        return <item.component
                                            {...routeProps}
                                            routes={item.children} />
                                    }
                                }
                            />
                        )
                    })
                }
            </Switch>
        </HomeLayout>
    );
}
