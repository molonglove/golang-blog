import React from "react";
import IconFont from "../../../../commons/CommonIcon";
import './index.less';

export default function ArticleFooter(props) {
    return (
	    <div className="content_footer">
		    <div className="end_thanks">
			    以上文章结束
			    <IconFont type="icon-tongxinyuan" style={{marginLeft: 10 + 'px', marginRight: 10 + 'px'}} />
			    感谢您的阅读
		    </div>
		    
		    <div className="end_btn_area">
			    <div className="end_commit">请多多留言，您的建议将大大提升我的创作质量</div>
			    <div className="btn_add">加我好友</div>
		    </div>
		    <div className="end_affirm">
			    <div className="affirm_item">
				    <div className="affirm_item_title">本文作者：</div>
				    <div className="affirm_item_value">星辰漫步者</div>
			    </div>
			    <div className="affirm_item">
				    <div className="affirm_item_title">本文链接：</div>
				    <div className="affirm_item_value">http://meblog.online/home/index/1</div>
			    </div>
			    <div className="affirm_item">
				    <div className="affirm_item_title">版权声明：</div>
				    <div className="affirm_item_value">本站所有文章除特别申明外，均采用 BY-NC-SA许可协议。转载请注明出处！</div>
			    </div>
		    </div>
	    </div>
    )
}