import React from "react";
import marked from 'marked'
import hljs from 'highlight.js'
import 'highlight.js/styles/atom-one-dark.css';
import './enyarin.less'

marked.setOptions({
	renderer: new marked.Renderer(),
	pedantic: false,
	gfm: true,
	tables: true,
	breaks: false,
	sanitize: false,
	smartLists: true,
	smartypants: false,
	xhtml: false,
	highlight: (code) => {
		return hljs.highlightAuto(code).value;
	}
});



export default class ArticleHtml extends React.Component{
	
	render() {
		let html = marked(this.props.content);
		return (
			<div className="enyRain" dangerouslySetInnerHTML={{__html: html}} />
		)
	}
	
	
}
