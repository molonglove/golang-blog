import React from "react";
import ArticleHtml from "./theme/article";
import ArticleFooter from "./footer/footer";
import {ArticleOneDetail} from '../../../api/home';
import IconFont from "../../../commons/CommonIcon";
import loading from '../../../commons/loading'
import {timeFormat} from "../../../utils/timeUtils";
import './index.less';

export default class HomeArticle extends React.Component {

    state = {
        article: {
            artLength: 0,
            categoryList: [
                {
                    id: 0,
                    title: ''
                }
            ],
            id: '',
            integralNum: 0,
            isOverhead: true,
            markdown: '',
            praiseNum: 0,
            readsNum: 0,
            reviewsNum: 0,
            synopsis: '',
            title: '',
            tags: [],
            url: 'http://localhost:3000/#/home/article/'
        }
    }

    componentDidMount() {
        this.loadArticleInfo()
    }

    // 获取文章信息
    loadArticleInfo = () => {
        // debugger;
        const mask = loading();
        const {id} = this.props.match.params
        ArticleOneDetail({id: id}).then(res => {
            console.log(res.data);
            this.setState({article: res.data})
            mask.close()
        }, err => {
            console.log(err)
        })
    }

    // https://blog.csdn.net/qq_36754767/article/details/100582036
    render() {
        const {article} = this.state;
        const cateLen = article.categoryList.length;
        const tagLen = article.tags.length;
        return (
            <div className="article_main">
                <div className="content_area">
                    <div className="content_head">
                        <div className="head_title">{article.title}</div>
                        <div className="head_field">
                            <div className="field_item">
                                <IconFont type="icon-riqi" style={{fontSize: 20 + 'px', marginRight: 5 + 'px'}}/>
                                {timeFormat(article.createTime)}
                            </div>
                            <div className="field_item">
                                <IconFont type="icon-leibie" style={{marginRight: 5 + 'px'}}/>
                                {
                                    article.categoryList.map((innerItem, index) => {
                                        if (cateLen - 1 === index) {
                                            return (<span key={index}>{innerItem.title}</span>)
                                        } else {
                                            return (<span key={index}>{innerItem.title} / </span>)
                                        }
                                    })
                                }
                            </div>
                            <div className="field_item">
                                <IconFont type="icon-word" style={{marginRight: 5 + 'px'}}/>
                                {article.tags.map((innerItem, index) => {
                                    if (tagLen - 1 === index) {
                                        return (<span key={index}>{innerItem}</span>)
                                    } else {
                                        return (<span key={index}>{innerItem} / </span>)
                                    };
                                })}
                            </div>
                        </div>
                    </div>
                    <ArticleHtml content={article.markdown}/>
                    <ArticleFooter url={article.url}/>
                </div>
                <div className="article_message">

                </div>
            </div>
        )
    }
}