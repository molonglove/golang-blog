import * as React from "react";
import {ArticleArchive} from "../../../api/home";
import {timeFormat} from '../../../utils/timeUtils';
import './index.less';
import {message} from "antd";

export default class HomeArchive extends React.Component {

	state = {
		years: [2018, 2019, 2020, 2021],
		list: [],
		total: 0,
		year: 2021
	}

	componentDidMount() {
		this.loadArchiveData(this.state.years[3]);
	}


	loadArchiveData = (year) => {
		ArticleArchive(year).then(res => {
			console.log("loadArchiveData", res);
			message.success("加载归档数据成功");
			this.setState({...res})
		}).catch(err => {
			message.error("加载数据失败");
			console.log(err);
		})
	}

	changeYear = (year) => {
		this.setState({year});
		this.loadArchiveData(year);
	}

	render() {
		// https://ma-jinyao.cn/archive/
		const {years, list, year, total} = this.state;


		return (
			<div className="archive-main">
				<div className="archive-title">
					{
						years.map((item, index) => {
							return (
								<div key={index} className={year == item ? 'title-item-area-active' : 'title-item-area'} onClick={() => this.changeYear(item)}>
									{item}
								</div>
							)
						})
					}
				</div>
				<div className="archive-content">
					<div className="content-year-top">
						<div className="year-top-flag">{year}</div>
						<div className="year-top-total">
							共写有
							<strong style={{color: '#1890ff'}}> {total} </strong>
							篇文章
						</div>
					</div>
					{
						list.map((item, index) => {
							return (
								<div className="archive-item" key={index}>
									<div className="archive-item-data">{timeFormat(item.createTime)}</div>
									<div className="archive-item-content">
										<div className="item-content-title">
											<a href="#">{item.title}</a>
										</div>
										<div className="item-content-main">
											{item.synopsis}
										</div>
									</div>
								</div>
							)
						})
					}
				</div>
			</div>
		)
	}
}