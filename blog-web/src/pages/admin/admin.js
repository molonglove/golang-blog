import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {AdminRoutes} from "../../router/admin";
import zhCN from 'antd/lib/locale/zh_CN';
import {ConfigProvider} from "antd";
import 'antd/dist/antd.css';
import AdminLayout from "./layout";
import AdminLogin from "./login";
import {connect} from "react-redux";

@connect(
    state => state.user
)
class AdminMain extends React.Component{

    render() {



        const { status } = this.props;

        console.log(this.props);

        return (
            <ConfigProvider locale={zhCN}>
                <Switch>
                    {
                        !status ?
                            <Route path="/admin/login" render={routeProps => <AdminLogin {...routeProps} />} /> :
                            <AdminLayout>
                                {
                                    AdminRoutes.map(item => {
                                        return (
                                            <Route
                                                path={item.path}
                                                key={item.path}
                                                exact={item.exact}
                                                onEnter={this.checkAuth}
                                                render={
                                                    routeProps => {
                                                        return <item.component
                                                            {...routeProps}
                                                            routes={item.children} />
                                                    }
                                                }
                                            />
                                        )
                                    })
                                }
                            </AdminLayout>
                    }
                    <Redirect from="/**" to="/admin/login" />
                </Switch>
            </ConfigProvider>
        );
    }


}

export default AdminMain;