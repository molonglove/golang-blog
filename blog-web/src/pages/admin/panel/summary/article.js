import React from "react";
import { Chart } from '@antv/g2';
import './chart.less';

export default class ArticleSummary extends React.Component {

    componentDidMount() {

        // const data = [
        //     { year: '1991', value: 3 },
        //     { year: '1992', value: 4 },
        //     { year: '1993', value: 3.5 },
        //     { year: '1994', value: 5 },
        //     { year: '1995', value: 4.9 },
        // ];
        const {data} = this.props;

        const chart = new Chart({
            container: 'c1',
            autoFit: true,
            height: 300,
            width: 600,
            autoFix: true
        });

        chart.data(data);
        chart.tooltip({
            showMarkers: false
        });

        chart
            .interval()
            .position('year*value')
            .color('year');

        chart.interaction('element-highlight');
        chart.interaction('element-list-highlight');
        chart.interaction('legend-highlight');
        chart.interaction('axis-label-highlight');

        chart.render();
    }


    render() {
        const {title} = this.props;
        return (
            <div className="charts-item" >
                <div className="chart-title">{title}</div>
                <div id="c1" className="chart-main" />
            </div>
        )
    }

}