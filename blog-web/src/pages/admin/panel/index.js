import React from "react";
import ArticleSummary from "./summary/article";
import './index.less';

export default class AdminPanel extends React.Component{


    render() {

        const data = [
            { year: '分类一', value: 30 },
            { year: '分类二', value: 40 },
            { year: '分类三', value: 35 },
            { year: '分类四', value: 25 },
            { year: '分类五', value: 49 },
            { year: '分类六', value: 29 },
        ];

        return (
            <div className="charts-area" >
                <div className="pic-top">
                    <ArticleSummary title="文章分类统计" data={data} />
                </div>
            </div>
        )
    }

}