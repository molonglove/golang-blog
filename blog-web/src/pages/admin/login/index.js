import React from "react";
import Wave from "./wave.png";
import Bg from "./bg.svg";
import Avatar from './avatar.svg';
import './index.less';
import IconFont from "../../../commons/CommonIcon";
import loading from "../../../commons/loading";
import {Login} from "../../../api/admin/user";
import {message} from "antd";
import {connect} from "react-redux";
import {setUserInfo} from "../../../redux/active/user";

@connect(
    state => state.user,
    {
        setUserInfo
    }
)
class AdminLogin extends React.Component {

    state = {
        flag: {
            username: false,
            password: false
        },
        fromData: {
            username: "long",
            password: "xmft2354"
        }

    }

    inputOnFocus = (event, name) => {
        const {flag, fromData} = this.state
        if (name === 'username') {
            let newFlag = Object.assign(flag, {username: true, password: fromData.password !== ''})
            this.setState({flag: newFlag})
        }
        if (name === 'password') {
            let newFlag = Object.assign(flag, {username: fromData.username !== '', password: true})
            this.setState({flag: newFlag})
        }
    }

    inputOnBlur = () => {
        const {flag, fromData} = this.state
        let newFlag = Object.assign(flag, {
            username: fromData.username !== '',
            password: fromData.password !== ''
        })
        this.setState({flag: newFlag})
    }

    inputChange = (event) => {
        const {fromData} = this.state
        let newFromData = Object.assign({}, fromData)
        let data = event.target.value;
        let name = event.target.name;
        if (name === 'username') {
            newFromData.username = data
        }
        if (name === 'password') {
            newFromData.password = data
        }
        this.setState({fromData: newFromData})
    }

    // 登陆按钮
    loginHandler = () => {
        const mask = loading();
        Login(this.state.fromData).then(res => {
            mask.close()
            if (res.code === '001001200') {
                this.props.setUserInfo({...res.data, status: true})
                message.success("登录成功")
                this.props.history.push('/admin/panel')
            } else {
                message.error(res.data)
            }
        }).catch(err => {
            console.log(err);
        })
    }

    render() {
        const {flag} = this.state;
        return (
            <div className="login_main">
                <img src={Wave} alt="" className="wave"/>
                <div className="container_area">
                    <div className="img">
                        <img src={Bg} alt="" style={{width: 500 + 'px'}}/>
                    </div>
                    <div className="login-content">
                        <div className="from-area">
                            <img src={Avatar} alt=""/>
                            <h2>欢迎登录</h2>
                            <div className="input-div">
                                <div className="input-icon">
                                    <IconFont type="icon-yonghu" style={{color: '#d9d9d9', fontSize: 1.4 + 'rem'}} />
                                </div>
                                <div className="input-label">
                                    <h5 className={flag.username ? 'input_focus_title' : 'input_title'}>用户名</h5>
                                    <input
                                        name="username"
                                        onChange={(event => this.inputChange(event))}
                                        onFocus={(event) => this.inputOnFocus(event, 'username')}
                                        onBlur={() => this.inputOnBlur('username')}
                                        type="text"/>
                                </div>
                            </div>
                            <div className="input-div">
                                <div className="input-icon">
                                    <IconFont type="icon-mima" style={{color: '#d9d9d9', fontSize: 1.4 + 'rem'}} />
                                </div>
                                <div className="input-label">
                                    <h5 className={flag.password ? 'input_focus_title' : 'input_title'}>密码</h5>
                                    <input
                                        name="password"
                                        onChange={(event => this.inputChange(event))}
                                        onFocus={(event) => this.inputOnFocus(event, 'password')}
                                        onBlur={() => this.inputOnBlur('password')}
                                        type="password" />
                                </div>
                            </div>
                            <div className="input-btn" onClick={this.loginHandler}>登录</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AdminLogin