import * as React from "react";
import './index.less';
import {Button} from "antd";
import IconFont from "../../../commons/CommonIcon";
import FolderImg from "./folder.png";
import WordImg from "./word.png";
import PdfImg from "./pdf.png";
import PngImg from "./Png.png";
import JpgImg from "./Jpg.png";
import ExcelImg from "./excel.png";
import PptImg from "./ppt.png";
import {TestUserStatus} from "../../../api/admin/article";

export default class AdminFile extends React.Component {

    // 获取文章信息
    loadArticleInfo = () => {
        TestUserStatus().then(res => {
           console.log(res);
        }, err => {
            console.log(err)
        })
    }


    componentDidMount() {
        this.loadArticleInfo();
    }

    render() {
        return (
            <div className="file_container">
                <div className="file_tools">
                    <div className="file_path_area">
                        <div className="path_title">路径：</div>
                        <div className="path_list">
                            <div className="path_item">日常文件</div>
                            <div className="path_item">2021-03-03</div>
                            <div className="path_item">问题统计</div>
                        </div>
                    </div>
                    <div className="file_tools_btn">
                        <div className="btn_file btn_primary"><IconFont type="icon-zengjia" style={{marginRight: 5 + 'px', color: '#fff'}} />创建目录</div>
                        <div className="btn_file btn_danger"><IconFont type="icon-shangchuan" style={{marginRight: 5 + 'px'}} />文件</div>
                        <div className="btn_file btn_primary"><input type="checkbox"/>全选</div>
                        <div className="btn_file">
                            <IconFont type="icon-Group-" style={{marginRight: 5 + 'px', fontSize: 1.2 + 'rem'}} />
                            <IconFont type="icon-hang" style={{marginRight: 5 + 'px', fontSize: 1.2 + 'rem'}} />
                        </div>
                    </div>
                </div>
                <div className="file_main">
                    <div className="folder_area">
                        <img src={FolderImg} alt="" className="folder_img"/>
                        <div className="folder_title">日常文件</div>
                    </div>
                    <div className="folder_area">
                        <img src={PdfImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={WordImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={WordImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={PngImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={WordImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={WordImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={WordImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={JpgImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={WordImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={WordImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={WordImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={ExcelImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={WordImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={WordImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={PptImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                    <div className="folder_area">
                        <img src={WordImg} alt="" className="folder_img"/>
                        <div className="folder_title">设计文档设计文档设计文档</div>
                    </div>
                </div>
            </div>

        )
    }

}