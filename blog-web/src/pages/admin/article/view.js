import React from "react";
import marked from "marked";
import hljs from "highlight.js";
import 'highlight.js/styles/atom-one-dark.css';
import './enyarin.less'
import {Button, Modal} from "antd";


marked.setOptions({
    renderer: new marked.Renderer(),
    pedantic: false,
    gfm: true,
    tables: true,
    breaks: false,
    sanitize: false,
    smartLists: true,
    smartypants: false,
    xhtml: false,
    highlight: (code) => {
        return hljs.highlightAuto(code).value;
    }
});

export default class ArticleView extends React.Component{


    render() {
        let html = marked(this.props.content);
        return (
            <Modal
                onCancel={() => this.props.handleCancel(false)}
                width={800}
                destroyOnClose={true}
                title="预览"
                visible={this.props.flag}
                footer={[
                    <Button
                        key="back"
                        type="primary"
                        onClick={() => this.props.handleCancel(false)}
                        danger>
                        关闭</Button>,
                ]}
            >
                <div className="enyRain" dangerouslySetInnerHTML={{__html: html}} />
            </Modal>
        )
    }
}