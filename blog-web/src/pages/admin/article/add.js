import * as React from "react";
import {Modal, Button, Form, Input, Select, Upload, message, Switch, Row, Col} from 'antd';
import {InboxOutlined} from '@ant-design/icons';
import TextArea from "antd/es/input/TextArea";

const {Dragger} = Upload;
const {Option} = Select;

export default class ArticleAdd extends React.Component {
	
	state = {
		loading: false,
		category: [
			{id: '1', title: '分类一'},
			{id: '2', title: '分类二'},
			{id: '3', title: '分类三'},
		],
		/** 文章表单数据 */
		articleForm: {
			file: '',
			title: '',
			categoryId: '',
			synopsis: '',
			tags: '',
			isDraft: false,
			isOverhead: false
		}
	}
	
	saveArticle = () => {
		this.props.handleOk(this.state.articleForm)
	}
	
	formValueChange = (changedValues, allValues) => {
		// console.log("formValueChange", changedValues, allValues);
		if (Object.keys(changedValues)[0] !== 'file') {
			this.setState({articleForm: {...this.state.articleForm, ...changedValues}})
		}
	}
	
	render() {
		const layout = {
			labelCol: {span: 4},
			wrapperCol: {span: 20},
		};
		
		const props = {
			name: 'file',
			multiple: true,
			maxCount: 1,
			action: '',
			beforeUpload: (file, fileList) => {
				let fileName = file.name
				if (fileName.split(".")[1] !== "md") {
					message.error('文章暂时只支持markdown的文档');
				} else {
					this.setState({articleForm: {...this.state.articleForm, file}})
				}
				return false;
			}
		};
		
		const {category} = this.props;
		
		return (
			<Modal
				title="增加文章"
				visible={this.props.flag}
				footer={[
					<Button
						key="back"
						type="primary"
						onClick={() => this.props.handleCancel(false)}
						danger>
						关闭</Button>,
					<Button
						key="submit"
						type="primary"
						loading={this.state.loading}
						onClick={this.saveArticle}>
						提交</Button>,
				]}
			>
				<Form
					{...layout}
					onValuesChange={this.formValueChange}
				>
					<Form.Item
						label="文章标题"
						name="title"
						rules={[{required: true, message: '文章标题不能为空！'}]}
					>
						<Input/>
					</Form.Item>
					<Form.Item
						label="文章分类"
						name="categoryId"
						rules={[{required: true, message: '文章标题不能为空！'}]}
					>
						<Select>
							{
								category.map((item, index) => {
									return <Option key={index} value={item.id}>{item.title}</Option>
								})
							}
						</Select>
					</Form.Item>
					<Form.Item
						label="文章标签"
						name="tags"
						rules={[{required: true, message: '文章标题不能为空！'}]}
					>
						<Input/>
					</Form.Item>
					<Form.Item
						label="上传文章"
						name="file">
						<Dragger {...props} maxCount={1}>
							<p className="ant-upload-drag-icon">
								<InboxOutlined/>
							</p>
							<p className="ant-upload-text">点击或者拖拽文档到该区域进行上传</p>
							<p className="ant-upload-hint">
								支持单次上传markdown文件，暂不支持其他文件上传
							</p>
						</Dragger>
					</Form.Item>
					<Form.Item
						label="文章简介"
						name="synopsis"
					>
						<TextArea rows={4}/>
					</Form.Item>
					<Row>
						<Col span={12}>
							<Form.Item
								labelCol={{ span: 8 }}
								wrapperCol={{ span: 14 }}
								label="文章顶置"
								name="isOverhead"
							>
								<Switch checkedChildren="开启" unCheckedChildren="关闭" />
							</Form.Item>
						</Col>
						<Col span={12}>
							<Form.Item
								labelCol={{ span: 8 }}
								wrapperCol={{ span: 14 }}
								label="存为草稿"
								name="isDraft"
							>
								<Switch checkedChildren="开启" unCheckedChildren="关闭" />
							</Form.Item>
						</Col>
					</Row>
				</Form>
			</Modal>
		)
	}
}