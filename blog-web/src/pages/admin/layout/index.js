import './index.css'
import * as React from "react";
import {withRouter} from 'react-router-dom'
import { Layout, Menu, Avatar, Popconfirm  } from 'antd';
import {AdminRoutes} from "../../../router/admin";
import  * as Icon from '@ant-design/icons';
import IconFont from "../../../commons/CommonIcon";
import {connect} from "react-redux";
import {setUserInfo} from "../../../redux/active/user";
const { Header, Sider, Content } = Layout;

const { SubMenu } = Menu;


@connect(
    state => state.user,
    {
        setUserInfo
    }
)
class AdminLayout extends React.Component {

    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    /**
     * 退出登录
     */
    logout = () => {
        this.props.history.push('/admin/login')
        this.props.setUserInfo({status: false})
        window.localStorage.clear()

    }

    render() {
        return (
            <Layout>
                <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
                    <div className="logo" />
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                        {AdminRoutes.map(item => {
                            if (item.children.length <= 0) {
                                return (
                                    <Menu.Item
                                        icon={<IconFont type={item.icon} />}
                                        key={item.path}
                                        onClick={p => this.props.history.push(p.key)}
                                    >{item.title}
                                    </Menu.Item>
                                )
                            } else {
                                return (
                                    <SubMenu key={item.path} title={item.title}
                                             icon={ <IconFont type={item.icon} />}
                                    >
                                        {item.children.map(innerItem => {
                                            return (
                                                <Menu.Item
                                                    icon={<IconFont type={innerItem.icon} />}
                                                    key={innerItem.path}
                                                    onClick={p => this.props.history.push(p.key)}
                                                >{innerItem.title}
                                                </Menu.Item>
                                            )
                                        })}
                                    </SubMenu>
                                )
                            }
                        })}
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Header className="site-layout-background" style={{ padding: '0px 16px' }}>
                        {React.createElement(this.state.collapsed ? Icon['MenuUnfoldOutlined'] : Icon['MenuFoldOutlined'], {
                            className: 'trigger',
                            onClick: this.toggle,
                        })}
                        <Popconfirm
                            placement="bottomRight"
                            title="现在退出系统？"
                            onConfirm={this.logout}
                            okText="退出"
                            cancelText="取消"
                        >
                            <Avatar
                                style={{
                                    backgroundColor: '#f56a00',
                                    verticalAlign: 'middle',
                                    position: 'absolute',
                                    right: 15 + 'px',
                                    top: 12 + 'px'
                                }}
                                size="large"
                                gap={4}
                            >
                                Long
                            </Avatar>
                        </Popconfirm>
                    </Header>
                    <Content
                        className="site-layout-background"
                        style={{
                            margin: '16px 16px',
                            padding: 16,
                            minHeight: 280,
                        }}
                    >
                        {this.props.children}
                    </Content>
                </Layout>
            </Layout>
        )
    }

}

export default withRouter(AdminLayout)