import * as React from "react";
import ReactDOM from 'react-dom';
import './index.less';
import IconFont from "../CommonIcon";

/**
 * 全局的loading加载框
 * @param icon
 * @param title
 * @returns {{close: (function(): void)}}
 */
export default function loading (icon = 'icon-jiazai', title = "正在加载......") {

    // No.1 再有
    const root = document.getElementById("root");
    const parent = document.createElement("div");
    parent.setAttribute("id", "loading");
    root.appendChild(parent);


    ReactDOM.render(
        <div className="loading_area">
            <div className="loading_icon"><IconFont type={icon} style={{fontSize: 30 + 'px', color: '#69c0ff'}} /></div>
            <div className="loading_title">{title}</div>
        </div>, parent);



    let close = () => {
        ReactDOM.unmountComponentAtNode(parent);
        if (document.body.contains(parent) !== undefined && document.body.contains(parent)) {
            root.removeChild(parent);
        }
    }


    return {
        close: () => close()
    }
}




