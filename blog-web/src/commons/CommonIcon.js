import {createFromIconfontCN} from "@ant-design/icons";
const IconFont = createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_2357337_2d3yae3ky6o.js',
});

export default IconFont;