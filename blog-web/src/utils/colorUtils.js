

/**
 * 随机颜色
 * @returns {string}
 * @constructor
 */
export const RandomColor = () => {
    const colorList = [
        "#ff7875",
        "#ff7a45",
        "#ffa940",
        "#faad14",
        "#7cb305",
        "#389e0d",
        "#08979c",
        "#1890ff",
        "#597ef7",
        "#9254de",
        "#f759ab"
    ]
    return colorList[Math.floor(Math.random() * colorList.length)]
}