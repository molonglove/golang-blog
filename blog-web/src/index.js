import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import {
    HashRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import { Provider } from 'react-redux';
import {persist, store} from "./redux/store";
import {PersistGate} from 'redux-persist/lib/integration/react';
import AdminMain from "./pages/admin/admin";
import HomeMain from "./pages/home/home";
import './index.less';
import './assets/css/reset.css'

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persist}>
            <Router>
                <Switch>
                    <Route path='/home' render={routeProps => <HomeMain {...routeProps} />} />
                    <Route path='/admin' render={routeProps => <AdminMain {...routeProps} />} />

                </Switch>
            </Router>
        </PersistGate>
    </Provider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
